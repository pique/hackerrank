function maxNonContiguous(array) {
    array = array.map(function (x) { return parseInt(x, 10); });
    var summation = [];
    var currentMax = array[0];
    var length = array.length;
    summation[0] = array[0];

    for (var i = 1; i < length; i++) {
        if (summation[i-1] > 0) {
            if (array[i] > 0) {
                summation[i] = summation[i-1] + array[i];
            } else {
                summation[i] = currentMax;
            }
        } else {
            summation[i] = array[i];
        }

        if (summation[i] > currentMax) {
            currentMax = summation[i];
        }

    }

    return currentMax;

}

function maxContiguous(array) {
    array = array.map(function (x) { return parseInt(x, 10); });
    var summation = [];
    var currentMax = array[0];
    var length = array.length;
    summation[0] = array[0];

    for (var i = 1; i < length; i++) {
        if (summation[i-1] > 0) {
            summation[i] = summation[i-1] + array[i];
        } else {
            summation[i] = array[i];
        }

        if (summation[i] > currentMax) {
            currentMax = summation[i];
        }

    }

    return currentMax;
}

function maxSubArray(array) {
    var maxContiguousSum = maxContiguous(array);
    var maxNonContiguousSum = maxNonContiguous(array);
    console.log(maxContiguousSum + ' ' + maxNonContiguousSum);
}

function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines.shift(), 10);
    for (var i = 0; i<lines.length ; i+=2) {
        maxSubArray(lines[i+1].split(' '));
    }
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});