function processData(input) {
    var lines = input.split('\n');
    var N = parseInt(lines[0].split(' ')[0]);
    var K = parseInt(lines[0].split(' ')[1]);
    K = K % N;
    var Q = parseInt(lines[0].split(' ')[2]);
    var ar = lines[1].split(' ');
    var rotated = ar.splice(N-K, K).concat(ar);
    var queries = lines.slice(2);

    queries.map(function(q) {
        console.log(rotated[parseInt(q)]);
    })
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});