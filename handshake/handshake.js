
function processData(input) {
    var lines = input.split('\n');
    var N = parseInt(lines.shift().split(' ')[0], 10);

    lines.map(function(people) {
        var remPeople = people - 1;

        var handshakes = (remPeople * (remPeople + 1)) / 2;

        console.log(handshakes);
    });
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});