function processData(input) {
    var lines = input.split('\n');
    var N = parseInt(lines[0]);
    var ar = lines[1].split(' ').map(function (x) { return parseInt(x, 10); });;
    var count = [], i = 0;
    
    for (i=0; i<100; i++) {
        count[i] = 0;
    }

    ar.map(function(v) {
        count[v]++;
    });

    console.log(count.join(' '));
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});