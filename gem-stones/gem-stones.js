function processData(input) {
    var hash = [];
    var localHash = [];
    var total = parseInt(input.split('\n')[0], 10);
    var rocks = input.split('\n').slice(1);
    var elemnts, elementCode;
    var gems = 0;

    rocks.map(function(rock) {
        elements = rock.split('');
        elements.map(function(element) {
            elementCode = element.charCodeAt(0); 
            if (!localHash[elementCode]) {
                localHash[elementCode] = 1;
            } 
        });

        localHash.map(function(element, index) {
            if (hash[index]) {
                hash[index] += 1;
            } else {
                hash[index] = 1;
            }
        });

        localHash = [];
        localHash.length = 0;
    });

    hash.map(function(element, index) {
        if (element === total) {
            gems++;
        }
    });
    console.log(gems);
} 

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});