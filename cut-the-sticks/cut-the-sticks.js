function processData(input) {
    var lines = input.split('\n');
    var N = lines[0];
    var ai = lines[1].split(' ');
    var aHash = [];

    ai.map(function(k, i) {
        var a = parseInt(k, 10); 
        aHash[a] = (aHash[a] || 0) + 1;
    });

    aHash.map(function(k, i) {
        console.log(N);
        N = N - k;
    });
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});