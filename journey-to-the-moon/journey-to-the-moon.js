function jounreyToTheMoon(N, edges) {
    var graph = new Graph(N);


    edges.map(function(edge) {
        var u = parseInt(edge.split(' ')[0], 10);
        var v = parseInt(edge.split(' ')[1], 10);
        graph.addEdge(u, v);
    })

    var cc = new CC(graph);
    var combinations = 0;
    var total = 0;
    
    total = 0;


    for(var i =0; i < cc.count; i++) {
        //console.log(i + " -- " + cc.compCount(i));

        var compSize = cc.compCount(i);

        var remSize = N - compSize;

        total += cc.compCount(i);

        combinations += compSize * remSize;

        N = remSize;
    }
    
    
    console.log(combinations);
}

function Graph(V) {
    /* Members */
    this.V = V;
    this.E = 0;
    this.adj = new Array(V);

    /* Init */
    for (var i = 0; i < V; i++) {
        this.adj[i] = [];
    }

    /* Methods */
    this.addEdge = function(v, w) {
        this.adj[v].push(w);
        this.adj[w].push(v);
        this.E++;
    };

    this.getAdjacents = function(v) {
        return this.adj[v];
    };

    this.print = function() {
        for (var v = 0; v < this.V; v++) {
            this.getAdjacents(v).map(function(adj, i) {
                console.log(v + ' -> ' + adj);
            });
        }
    };
}

function CC(G) {
        /* Members */
        this.count = 0;
        this.compCount = [];
        this.marked = [];
        this.id = [];
        
        /* Public */
        this.getId = function(v) {
            return this.id[v];
        };

        this.print = function() {
            this.id.map(function(cc, v) {
                console.log(v + ' - ' + cc);
            });
        };

        this.compCount = function(v) {
            return this.compCount[v];
        };

        this.vertixCount = function(v) {
            return this.compCount[this.id[v]];
        };

        /* Private */
        this._dfs = function(G, v) {
            var self = this;
            this.marked[v] = true;
            this.id[v] = this.count;
            
            if (this.compCount[this.count]) {
                this.compCount[this.count] += 1;    
            } else {
                this.compCount[this.count] = 1;        
            }
            
            G.getAdjacents(v).map(function(adj) {
                if (!self.marked[adj]) {
                    self._dfs(G, adj)
                };
            });
        }

        /* Init */
        for (var v = 0; v < G.V; v++){
            this.marked[v] = false;
        };

        for (var v = 0; v < G.V; v++){
            if (!this.marked[v]) {
                this._dfs(G, v);
                this.count++;
            }
        };

        

    }

function processData(input) {
    var lines = input.split('\n');
    var N = parseInt(lines.shift().split(' ')[0], 10);

    jounreyToTheMoon(N, lines);
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});