
function processData(input) {
    var lines = input.split('\n');
    var N = parseInt(lines.shift().split(' ')[0], 10);

    lines.map(function(line) {
        var socks = parseInt(line, 10);
        console.log(socks + 1);
    });
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});