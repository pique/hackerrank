function processData(input) {
    var lines = input.split('\n');
    var result = 0;
    var numbers = lines.slice(1);

    numbers.map(function(number) {
        result = 0;

        number.split('').map(function(digit) {
            if ((number % digit) == 0) {
                result = result + 1;
            }
        });

        console.log(result);
    });
}
var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});