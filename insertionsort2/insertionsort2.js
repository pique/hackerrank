function processData(input) {
    var N = parseInt(input.split('\n')[0], 10) - 1;
    var ar = input.split('\n')[1].split(' ');
    var i, j, c, swapped;
    var swap = function(array, i, j) {
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    };

    ar.map(function(v, i){
        ar[i] = parseInt(v, 10);
    })

    for(i=1; i<=N; i++) {
        c = ar[i];
        for(j=i-1; j>=0; j--) {
            if (c < ar[j]) {
                ar[j+1] = ar[j]
            } else {
                break;
            }
        }
        ar[j+1] = c;
        console.log(ar.join(' '));
    }
}


var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});