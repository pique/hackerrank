function processData(input) {
    var lines = input.split('\n');
    var N = lines.shift();
    

    for(var i=0; i<N; i++) {
        var towns = lines[(i * 2)+1].split(' ');
        var routes = towns.shift();

        towns.map(function(town) {
            routes *= town;
            routes %= 1234567;
        });

        console.log(routes);
    }
    
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});