function processData(input) {
    var parse_fun = function(s) {
        return parseInt(s, 10);
    };

    var lines = input.split('\n');
    var T = parse_fun(lines.shift());

    var data = lines.splice(0, T).map(parse_fun);
    var utopian = [];
    utopian[0] = 1;
    var i = 1;

    for (i = 1; i <= 60; i++) {
        if ((i % 2) != 0) {
            utopian[i] = utopian[i-1] * 2;
        } else {
            utopian[i] = utopian[i-1] + 1;
        }
    }

    data.map(function(N) {
        console.log(utopian[N]);
    });
    
}
var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});