function processData(input) {
    var N = input.split('\n')[0].split(' ')[0];
    var K = parseInt(input.split('\n')[0].split(' ')[1])
    var items = input.split('\n')[1].split(' ').map(function (x) { return parseInt(x, 10); });
    var count = 0;
    items = items.sort(function(a, b){return a - b;});

    
    items.map(function(k) {
        if (k < K) {
            count = count + 1;
            K = K - k;
        }
    });

    console.log(count);
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});