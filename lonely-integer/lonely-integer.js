function processData(input) {
    var lines = input.split('\n');
    var N = lines[1].split(' ');
    var xorValue = null;
    
    N.map(function(n) {
        n = parseInt(n, 10);
        if (!xorValue) {
            xorValue = n;
        } else {
            xorValue = xorValue ^ n;
        }
    });

    console.log(xorValue);
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});