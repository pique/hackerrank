function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines.shift(), 10);


    lines.map(function(input) {
        var count = 0;
        input.split('').reduce(function(p, n) {
            if (p === n) {
                count++;
            }
            return n;
        })
        console.log(count);
    });
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});