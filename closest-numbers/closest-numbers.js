function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines.shift(), 10);
    var numbers = lines[0].split(' ');
    var min = null;
    var diff = [];
    var result = [];
    numbers = numbers.sort(function(a, b){ return a - b});

    numbers.reduce(function(p, n) {
        if (!min) {
            min = Math.abs(p - n);
            result.push(p + ' ' + n);
        } else if (min > Math.abs(p - n)) {
            result = [];
            min = Math.abs(p - n);
            result.push(p + ' ' + n);
        } else if (min === Math.abs(p - n)) {
            result.push(p + ' ' + n);
        }
        return n;
    });

    console.log(result.join(' '));
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});