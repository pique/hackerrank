function processData(input) {
    var a1 = input.split('\n')[0].split('');
    var a2 = input.split('\n')[1].split('');
    var a1Map = {};
    var diff = 0;

    a1.map(function(c) {
        if (a1Map[c]) {
            a1Map[c] = a1Map[c] + 1;
        } else {
            a1Map[c] = 1;
        };
    });

    a2.map(function(c) {
        if (a1Map[c]) {
            a1Map[c] = a1Map[c] - 1;
        } else {
            a1Map[c] = -1;
        }
    });

    for (var key in a1Map) {
        if (a1Map.hasOwnProperty(key)) {
            diff = diff + Math.abs(a1Map[key]);
        }
    }

    console.log(diff);

}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});