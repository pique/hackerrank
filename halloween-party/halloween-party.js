function processData(input) {
    var lines = input.split('\n');
    var T = lines[0];
    var K = lines.slice(1);

    K.map(function(k) {
        var n = parseInt(parseInt(k, 10) / 2);
        var m = k - n;

        console.log(n * m);
    });
} 

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});