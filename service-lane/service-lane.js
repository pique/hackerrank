function processData(input) {
    var lines = input.split('\n');
    var N = lines[0].split(' ')[0];
    var T = lines[0].split(' ')[1];
    var lane = lines[1].split(' ');
    var ij = lines.slice(2);
    var width = [];
    width[1] = [];
    width[2] = [];
    width[3] = [];

    lane.map(function(length, index) {
        
        [1, 2, 3].map(function(vec) {
            width[vec][index] = width[vec][index] || 0;
            if (length == vec) {
                if (width[vec][index - 1]) {
                    width[vec][index] = width[vec][index - 1] + 1;    
                } else {
                    width[vec][index] = 1;
                }
                
            } else {
                width[vec][index] = width[vec][index - 1] || 0;
            }
        })
        
    });

    ij.map(function(path) {
        var start = parseInt(path.split(' ')[0], 10);
        var end = parseInt(path.split(' ')[1], 10);

        if ((width[1][end] - (width[1][start-1] || 0)) > 0) {
            console.log("1");
        } else if ((width[2][end] - (width[2][start-1] || 0)) > 0) {
            console.log("2");
        } else {
            console.log("3")
        }

    })

}
var fs = require('fs');
fs.readFile('input4', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});