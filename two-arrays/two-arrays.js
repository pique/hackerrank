function twoArrays(a1, a2, N, K) {
    var i = 0, sum=[];
    a1 = a1.sort(function(a, b){return a - b;});
    a2 = a2.sort(function(a, b){return a - b;}).reverse();

    for (i=0; i<N; i++) {
        if ((a1[i] + a2[i]) < K) {
            return 'NO';
        }

    }

    return 'YES';
}

function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines[0], 10);
    var i = 0, a1, a2, N, K, iP;

    for (i=0; i<T; i++) {
        iP = (i * 3) + 1;
        
        N = parseInt(lines[iP].split(' ')[0]);
        K = parseInt(lines[iP].split(' ')[1]);

        a1 = lines[iP+1].split(' ').map(function (x) { return parseInt(x, 10); });
        a2 = lines[iP+2].split(' ').map(function (x) { return parseInt(x, 10); });
        console.log(twoArrays(a1, a2, N, K));
    }
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});