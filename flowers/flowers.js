function processData(input) {
    var N = input.split('\n')[0].split(' ')[0];
    var K = input.split('\n')[0].split(' ')[1];
    var C = input.split('\n')[1].split(' ');
    var flowers = [];
    var minsum = 0;
    C.sort(function(a, b) {
        return parseInt(b) - parseInt(a);
    });

    for(var i =0; i < K; i++) {
        flowers[i] = [];
    }

    for(var i =0; i < N; i++) {
        var length = flowers[i % K].length;
        flowers[i % K][length] = C[i];
    }

    for(var i =0; i < N; i++) {
        for(var j =0; j < K; j++) {
            if (flowers[j][i]) {
                minsum += (i + 1) * flowers[j][i];
            }
        }
    }

    console.log(minsum);

} 

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});