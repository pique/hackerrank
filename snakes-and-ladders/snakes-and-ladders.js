function snakesAndLadders(ladders, snakes) {
    var board = getNewBoard(),
        laddersMap = {},
        snakesMap = {};

    ladders.map(function(ladder) {
        var steps = ladder.split(',');
        var start = parseInt(steps[0], 10);
        var end = parseInt(steps[1], 10);

        laddersMap[start] = end;
        board.addEdge(start, end);
    });

    snakes.map(function(snake) {
        var steps = snake.split(',');
        var start = parseInt(steps[0], 10);
        var end = parseInt(steps[1], 10);

        snakesMap[start] = end;
        board.addEdge(start, end);
    });
    //board.print();
    var path = findShortestPath(board);
    
    var laddersInPath = 0;
    var snakesInPath = 0;

    path.reduce(function(a, b) {
        if (laddersMap[a] === b) {
            laddersInPath++;
        }
        return b;
    });

    path.reduce(function(a, b) {
        if (snakesMap[a] === b) {
            snakesInPath++;
        }
        return b;
    });

    console.log(path.length - laddersInPath - snakesInPath - 1);
}

function getNewBoard() {
    var board = new Array(101);
    var bGraph = new Graph(100);

    for (var v = 1; v <= 100; v++) {
        bGraph.addEdge(v, v + 1);
        bGraph.addEdge(v, v + 2);
        bGraph.addEdge(v, v + 3);
        bGraph.addEdge(v, v + 4);
        bGraph.addEdge(v, v + 5);
        bGraph.addEdge(v, v + 6);
    }

    return bGraph;
};

function findShortestPath(board) {
    var bGraph = new bfsSearch(board, 1);

    return bGraph.pathTo(100);
};

function Graph(V) {
    /* Members */
    this.V = V;
    this.E = 0;
    this.adj = new Array(V);

    /* Init */
    for (var i = 0; i < V; i++) {
        this.adj[i] = [];
    }

    /* Methods */
    this.addEdge = function(v, w) {
        if (v < 0 || w < 0) {
            return;
        }
        if (v > 100 || w > 100) {
            return;
        }
        this.adj[v].push(w);
        this.E++;
    };

    this.getAdjacents = function(v) {
        return this.adj[v];
    };

    this.print = function() {
        for (var v = 0; v < this.V; v++) {
            this.getAdjacents(v).map(function(adj, i) {
                console.log(v + ' -> ' + adj);
            });
        }
    };
}

function bfsSearch(G, S) {
    /* Members */
    this.marked = [];
    this.edgeTo = [];
    this.S = S;

    this.bfs = function(G, v) {
        var queue = [],
            x;
        queue.push(this.S);


        while (queue.length > 0) {
            x = queue.shift();

            this.marked[x] = true;

            var adjs = G.getAdjacents(x),
                self = this;

            adjs && adjs.map(function(adj) {
                if (!self.marked[adj]) {
                    queue.push(adj);
                    self.marked[adj] = true;
                    self.edgeTo[adj] = x;
                }
            });
        }
    };

    this.hasPathTo = function(v) {
        return !!this.marked[v];
    };

    this.pathTo = function(v) {
        var path = [];

        if (!this.hasPathTo(v)) {
            return path;
        }

        for (var x = v; x != this.S; x = this.edgeTo[x]) {
            path.push(x);
        }

        path.push(this.S);

        return path.reverse();
    };

    this.print = function() {
        console.log(this.marked);
    };

    /* Init */
    this.bfs(G, S);
}

function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines.shift(), 10);
    var i = 0,
        a1, a2, N, K, iP;

    for (i = 0; i < T; i++) {
        iP = (i * 3);

        ladders = lines[iP + 1].split(' ');
        snakes = lines[iP + 2].split(' ');
        snakesAndLadders(ladders, snakes);
    }
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});