function nCrTable(n) {
    var table = [];
    table[0] = 1;

    for(var i=1; i<=n; i++) {
        table[i] = (table[i-1] * (n - i + 1)) / i;
    }

    for(var i=0; i<=n; i++) {
        table[i] %= 1000000000;
        table[i] = Math.round(table[i]);
    }
    

    console.log(table.join(' '));
}

function processData(input) {
    var lines = input.split('\n');
    var N = lines.shift();

    lines.map(function(n) {
        nCrTable(n);
    });
    
}



var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});