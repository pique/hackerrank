function processData(input) {
    var lines = input.split('\n');
    var a = lines[1].split(' ');
    var b = lines[3].split(' ');
    var aMap = [];
    var bMap = [];
    var missingNumbers = [];

    a.map(function(number) {
        if (aMap[number]) {
            aMap[number] += 1;
        } else {
            aMap[number] = 1;
        }
    });

    b.map(function(number) {
        if (bMap[number]) {
            bMap[number] += 1;
        } else {
            bMap[number] = 1;
        }
    });
    
    bMap.map(function(val, index) {
        if (!aMap[index]) {
            missingNumbers.push(index);
        } else {
            if (val > aMap[index]) {
                missingNumbers.push(index);
            }
        }
    });


    console.log(missingNumbers.join(' '));
    
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});