function processData(input) {
    var N = parseInt(input.split('\n')[0], 10) - 1;
    var ar = input.split('\n')[1].split(' ');
    var x = ar[N];

    if (N == 0) {
        console.log(ar[0]);
        return;
    }

    for (var i = N - 1; i >= 0; i--) {
        if (ar[i] > x) {
            ar[i + 1] = ar[i];
        } else {
            break;
        }

        console.log(ar.join(' '));
    }
    ar[i + 1] = x;
    console.log(ar.join(' '));
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});