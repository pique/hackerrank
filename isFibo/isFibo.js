function processData(input) {
    var lines = input.split('\n');
    var N = lines.slice(1);
    var fiboSeries = [];
    var i = 2;
    var calculateFiboSeries = function(n) {
        var cFibo = 1;
        while (cFibo < n) {
            cFibo = fiboSeries[i - 1] + fiboSeries[i - 2];
            fiboSeries[i] = cFibo;
            i++;
        }
    }
    fiboSeries[0] = 0;
    fiboSeries[1] = 1;
    calculateFiboSeries(10000000000);

    N.map(function(value) {
        var number = parseInt(value, 10);
        if (fiboSeries.indexOf(number) > 0) {
            console.log('IsFibo');
        } else {
            console.log('IsNotFibo');
        }
    });
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});