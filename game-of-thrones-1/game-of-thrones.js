function processData(input) {
    var str = input.split('\n')[0].split('');
    var mesh = {};
    var odds = 0;
    var N = str.length;

    str.map(function(c) {
        mesh[c] = (mesh[c] || 0) + 1;
    });

    for (var c in mesh) {
        if (mesh[c] % 2 === 1) {
            odds += 1;
        }
    }

    if ((N % 2 === 0) && (odds === 0)) {
        console.log("YES");
    } else if ((N % 2 === 1) && (odds === 1)) {
        console.log("YES");
    } else {
        console.log("NO");
    }
} 

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});