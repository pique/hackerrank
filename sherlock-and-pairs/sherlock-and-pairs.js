function sherlockPairs(array) {
    var map = [], pairs = 0;

    array.map(function(x) {
        if (map[x]) {
            map[x]++;
        } else {
            map[x] = 1;
        }
    });

    map.map(function(x) {
        pairs = pairs + (x * (x - 1));
    });

    return pairs;
}

function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines[0], 10), i;

    for (i=1; i<=T; i++) {
        console.log(sherlockPairs(lines[(i*2)].split(' ').map(function (x) { return parseInt(x, 10); })));
    }
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});