function processData(input) {
    var lines = input.split('\n');
    var N = parseInt(lines.shift(), 10);
    var K = parseInt(lines.shift(), 10);
    var smallest = 0, current = 0;
    lines = lines.sort(function(a, b){return a - b;});


    for (var i =0; i < N; i++) {
        if (i + K - 1 === N) {
            break;
        }

        current = lines[i + K - 1] - lines[i];

        if (i === 0) {
            smallest = current;
        }

        if (current < smallest) {
            smallest = current;
        }
    }

    console.log(smallest);

}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});