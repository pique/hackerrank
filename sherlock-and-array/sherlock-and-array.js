function sherlockArray(array) {
    var left, right;
    var i = 0, length = array.length;

    for (i=1; i<length; i++) {
        array[i] = array[i] + array[i-1];
    }
    
    for (i=0; i<length; i++) {
        left = 0;
        right = 0;

        if (i == 0) {
            left = 0;
        } else {
            left = array[i-1];
        }

        if (i === (length - 1)) {
            right = 0;
        } else {
            right = array[length-1] - array[i];
        }

        if (left === right) {
            return 'YES';
        }
    }
    return 'NO';
}

function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines[0], 10), i;

    for (i=1; i<=T; i++) {
        console.log(sherlockArray(lines[(i*2)].split(' ').map(function (x) { return parseInt(x, 10); })));
    }
}

var fs = require('fs');
fs.readFile('data', { encoding: 'ascii' }, function (err, data) {
  if (err) throw err;
  processData(data);
});