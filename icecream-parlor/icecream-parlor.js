function iceCreamParlour(M, iceCreams) {
    var iMap = [];
    var result = [];
    var N = iceCreams.length;
    
    iceCreams.map(function(iceCream, index) {
        if (!iMap[iceCream]) {
            iMap[iceCream] = [];
        }
        iMap[iceCream].push(index + 1);
    });

    for(var i = 0; i < N; i++) {
        var remaining = M - iceCreams[i];
        if (!iMap[remaining]) {
            continue;
        }

        if (iMap[remaining].length > 1) {
            result = iMap[remaining];
            break;
        } else {
            if (iMap[remaining][0] === i + 1) {
                continue;
            }
            result.push(i + 1);
            result.push(iMap[remaining].shift());
        }

        break;
    };

    console.log(result.join(' '));

}

function processData(input) {
    var lines = input.split('\n');
    var T = parseInt(lines.shift(), 10);
    var i = 0, M, iceCreams;

    for (i = 0; i < T; i++) {
        iP = (i * 3);

        M = parseInt(lines[iP], 10);
        iceCreams = lines[iP+2].split(' ');
        iceCreamParlour(M, iceCreams);
    }
}

var fs = require('fs');
fs.readFile('data', {
    encoding: 'ascii'
}, function(err, data) {
    if (err) throw err;
    processData(data);
});